var express = require('express');
var bodyParser = require('body-parser');
var app = express();
var server = require('http').Server(app);
var socket = require('socket.io')(server);
var alumnos = require('./daoALumnos/alumnos');
app.use(express.static('public'));
var router = express.Router();
var alumnosOrm = require('./models/Alumnos');
app.use(bodyParser.urlencoded({extended: false}));
app.use(bodyParser.json());
router.get('/usuarios', function (req, res) {
    alumnos.dameAlumnos(function (error, data) {
        res.status(200).send(data);
    });
});
router.post('/guardarUsuarios', function (req, res) {
    var params = req.body;
    alumnosOrm.guardar(params, function (erro, data) {
        setTimeout(function () {
            if (data == 1) {
                alumnos.dameAlumnos(function (error, data) {
                    console.log("el dato de los usuarios es ");
                    console.log(data);
                    res.status(200).send(data);
                    socket.emit('messages');
                });
            }
        });
    });
});


socket.on('connection', function (socket) {
    console.log('Alguien se ha conectado con Sockets');
//    //NOS VA A SERVIR PARA ENVIAR O EMITIR SOCKETS
    socket.emit('messages');
    socket.on('new-message', function () {
        console.log("entro al mensaje emitido");
        socket.emit('messages');
    });
//
//
});




app.use('/api', router);
server.listen('3333', function () {
    console.log("Servidor levantado satisfactoriamente");
    console.log("http://localhost:3333/api");
});