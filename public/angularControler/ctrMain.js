app.controller('ctrMain', function ($scope, $http) {
    $scope.usuarios = {};
    $scope.usuarios.idUsuario = 0;
    $scope.usuarios.nombre = "";
    $scope.usuarios.correo = "";
    $scope.usuarios.telefono = "";
    var socket = io();
////    INICIAMOS EL SOCKET Y LE DAMOS UN NOMBRE
    socket.on('messages', function () {
        setTimeout(function () {
            $scope.dameUsuarios();
        });
    });
    $scope.dameUsuarios = function () {
        $http.get("http://localhost:3333/api/usuarios").success(function (data) {
            console.log(data);
            $scope.listaAlumnos = data;
        });
    };
    $scope.dameUsuarios();
    $scope.guardarUsuarios = function () {
        $http.post("http://localhost:3333/api/guardarUsuarios", $scope.usuarios).success(function (respuesta) {
            setTimeout(function () {
                console.log(respuesta);
                $scope.listaAlumnos = respuesta;
                socket.emit('new-message');
            });
        });
    };
});