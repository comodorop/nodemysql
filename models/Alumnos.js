var Sequelize = require('sequelize');
var sequelize = new Sequelize('eventos', 'root', '', {
    dialect: 'mysql',
    define: {
        timestamps: false
    }
});
var alumnos = sequelize.define('alumnos', {
    idAlumno: {
        type: Sequelize.INTEGER,
        autoIncrement: true,
        primaryKey: true
    },
    nombre: {
        type: Sequelize.STRING
    },
    telefono: {
        type: Sequelize.STRING
    },
    correo: {
        type: Sequelize.STRING
    },
    idDia: {
        type: Sequelize.INTEGER
    },
    idHorario: {
        type: Sequelize.INTEGER
    }

});

function test() {
    console.log("Entrando");
    sequelize.authenticate().then(function (err) {
        console.log('Connection has been established successfully.');
    }).catch(function (err) {
        console.log('Unable to connect to the database:', err);
    });
}

function guardar(objAlumnos, callback) {
    alumnos.create(objAlumnos).then(function (result) {
        callback(null, 1);
    });
}

module.exports = {
    test,
    guardar
};